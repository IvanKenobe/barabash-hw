const request = new XMLHttpRequest()
request.open("GET", "https://swapi.dev/api/films/")
request.responseType = "json"
request.send()
request.onload = function () {
    if (request.status >= 300){
        console.log(`Error ${request.status} ${request.statusText}`)
    } else {
        request.response.results.forEach(elem => {
            const filmList = document.createElement("ul")
            const filmTitle = document.createElement("li")
            const filmId = document.createElement("li")
            const filmDescription = document.createElement("li")
            const charactersList = document.createElement("ul")
            filmTitle.textContent = elem.title
            filmId.textContent = `Episode ${elem.episode_id}`
            filmDescription.textContent = `Film Description: ${elem.opening_crawl}`
            let arr = []
            elem.characters.forEach(item => {
                const newRequest = new XMLHttpRequest()
                newRequest.open("Get", item)
                newRequest.responseType = "json"
                newRequest.send()
                newRequest.onload = function () {
                    if (newRequest.status < 300) {
                        arr.push(`<li>${newRequest.response.name}</li>`)
                        if (arr.length === elem.characters.length){
                            charactersList.innerHTML = arr.join("")
                        }
                    } else {
                        console.log(`Error ${newRequest.status} ${newRequest.statusText}`)
                    }
                }
            })
            filmList.append(filmTitle)
            filmList.append(filmId)
            filmList.append(filmDescription)
            filmList.append(charactersList)
            document.body.append(filmList)
        })

    }
}


