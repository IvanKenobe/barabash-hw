    function HamburgerException (message) {
        this.message = message;
        this.name = 'HamburgerException';
    }

    function Hamburger(size, stuffing) {
        try {
            if (!size) {
                throw new HamburgerException("You didn't mention the size");
            } else if (!stuffing) {
                throw new HamburgerException("You didn't mention the stuffing");
            } else {
                this.size = size;
                this.stuffing = stuffing;
                this.topping = [];
            }
        } catch (e) {
            console.error(e.name + ": " + e.message);
        }
    }

    Hamburger.SIZE_SMALL = {
        name: "SIZE_SMALL",
        size:"small",
        price:50,
        callories:20
    };
    Hamburger.SIZE_LARGE = {
        name: "SIZE_LARGE",
        size:"large",
        price:100,
        callories:40
    };
    Hamburger.STUFFING_CHEESE = {
        name: "STUFFING_CHEESE",
        stuffing:"cheese",
        price:10,
        callories:20
    };
    Hamburger.STUFFING_SALAD = {
        name: "STUFFING_SALAD",
        stuffing:"salad",
        price:20,
        callories:5
    };
    Hamburger.STUFFING_POTATO = {
        name: "STUFFING_POTATO",
        stuffing:"potato",
        price:15,
        callories:10
    };
    Hamburger.TOPPING_MAYO = {
        topping:"mayonnaise",
        price:20,
        callories:5
    };
    Hamburger.TOPPING_SPICE = {
        topping:"spicy",
        price:15,
        callories:0
    };

    Hamburger.prototype.addTopping = function (topping) {
        try {
            if (this.topping.includes(topping)) {
                throw new HamburgerException("This topping is already added.")
            } else {
                this.topping.push(topping);
            }
        } catch (e) {
            console.error(e.name + " " + e.message);
        }
    };

    Hamburger.prototype.removeTopping = function (topping) {
        try {
            var index = this.topping.indexOf(topping);
            if (index !== "-1") {
                this.topping.splice(index, 1)
            }
            throw new HamburgerException("There is no such toppings in the list")
        } catch (e) {
            console.error(e.name + ": " + e.message);
        }
    };

    Hamburger.prototype.getToppings = function () {
        try {
            var arr = [];
            this.topping.map(elem => arr.push(elem.topping))
            if (arr[0] == undefined) {
                throw new HamburgerException("В вашем гамбургере нету топпингов!");
            } else {
                return arr;
            }
        } catch (e) {
            console.error(e.name + ": " + e.message);
        }
    };

    Hamburger.prototype.getSize = function () {
        return this.size.name;
    };

    Hamburger.prototype.getStuffing = function () {
        return this.stuffing.name;
    };

    Hamburger.prototype.calculatePrice = function () {
        var sum = 0;
        for (var item in this){
            var hamburgerElem = this[item];
            if (typeof hamburgerElem != "function"){
                if (hamburgerElem.price){
                    sum += hamburgerElem.price;
                } else if (hamburgerElem instanceof Array){
                    for (var newItem in hamburgerElem){
                        sum += hamburgerElem[newItem].price;
                    }
                }
            }
        }
        return sum;
    };

    Hamburger.prototype.calculateCalories = function () {
        var sum = 0;
        for (var item in this){
            var hamburgerElem = this[item];
            if (typeof hamburgerElem != "function"){
                if (hamburgerElem.callories){
                    sum += hamburgerElem.callories;
                } else if (hamburgerElem instanceof Array){
                    for (var newItem in hamburgerElem){
                        sum += hamburgerElem[newItem].callories;
                    }
                }
            }
        }
        return sum;
    };

    var hamburger = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_SALAD);
    hamburger.addTopping(Hamburger.TOPPING_SPICE);
    hamburger.addTopping(Hamburger.TOPPING_MAYO)
    hamburger.addTopping(Hamburger.STUFFING_CHEESE)
    hamburger.removeTopping(Hamburger.STUFFING_SALAD)

    console.log(hamburger.calculatePrice());
    console.log(hamburger.calculateCalories());
    console.log(hamburger.getToppings());
    console.log(hamburger.getStuffing());
    console.log(hamburger.getSize())
