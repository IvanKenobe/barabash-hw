class Hamburger {

    static SIZE_SMALL = {
        name: "small",
        price: 50,
        calories: 20
    }
    static SIZE_LARGE = {
        name: "BIG",
        price: 100,
        calories: 20
    };

    static STUFFING_CHEESE = {
        name: "CHEESE",
        price: 10,
        calories: 20
    }

    static STUFFING_SALAD = {
        name: "SALAD",
        price: 20,
        calories: 5
    }

    static STUFFING_POTATO = {
        name: "POTATO",
        price: 15,
        calories: 10
    }

    static TOPPING_MAYONNAISE = {
        name: "MAYONNAISE",
        price: 20,
        calories: 5
    }

    static TOPPING_SPICE = {
        name: "SPICE",
        price: 15,
        calories: 0
    }

    constructor(size, stuffing) {
        this.size = size;
        this.stuffing = stuffing;
        this.toppings = []
        if (!size) throw new Error("No Size")
        if (!stuffing) throw new Error("No stuffing")
    }
    addTopping (topping) {
        if (!topping) throw new Error("NoTopping")
        if (this.toppings.indexOf(topping) !== -1) throw new Error("You have added this topping")
        if (this.toppings.indexOf(topping) === -1){
            this.toppings.push(topping)
        }
    }
    removeTopping(topping) {
        if (!topping) throw new Error("Nothing to remove")
        if (this.toppings.indexOf(topping) !== -1) {
            this.toppings.splice(this.toppings.indexOf(topping),1)
        }

    }
    getToppings(){
        return this.toppings
    }
    getSize(){
        return this.size.name
    }
    getStuffing () {
        return this.stuffing.name
    }
    calculatePrice () {
        let price = 0;
        price += this.size.price + this.stuffing.price
        this.toppings.forEach(elem => price += elem.price)
        return price
    }
    calculateCallories () {
        let calories = 0;
        calories += this.size.calories + this.stuffing.calories
        this.toppings.forEach(elem => calories += elem.calories)
        return calories
    }





}
let burger = new Hamburger(Hamburger.SIZE_LARGE, Hamburger.STUFFING_CHEESE)
burger.addTopping(Hamburger.STUFFING_SALAD)
burger.addTopping(Hamburger.STUFFING_POTATO)
burger.removeTopping(Hamburger.STUFFING_POTATO)
console.log(burger.calculatePrice())
console.log(burger.calculateCallories())
console.log(burger.getSize())
console.log(burger.getToppings())
console.log(burger.getStuffing())
console.log(burger)

