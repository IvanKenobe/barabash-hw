const playBtn = document.getElementById("btnPlay");
const level = document.getElementById("difficult");
const cells = document.querySelectorAll(".element");

class Game {
    constructor(level, playBtn, cells) {
        this.playBtn = playBtn
        this.cells = cells
        this.level = level;
        this.userScore = 0;
        this.computerScore = 0;
        this.finishPoint = cells.length/2;

    }

    getRandomCell(){
        const freeHoles = [...cells].filter(elem => !elem.classList.contains("green") && !elem.classList.contains("red"))
        const index = Math.floor(Math.random() * freeHoles.length)
        this.currentCell = freeHoles[index]
        return this.currentCell
    }

    changeDifficult() {
        const levelDifficult = this.level.selectedIndex
        if (levelDifficult === 0) this.level = 1500
        else if (levelDifficult == 1) this.level = 1000
        else if (levelDifficult == 2) this.level = 500
        return this.level

    }

    getGameTimer(){
        const cell = this.getRandomCell(this.cells)
        cell.classList.add("blue")
            this.timer= setTimeout(() => {
                if(cell.classList.contains("green")){
                    this.userScore++
                } else if (cell.classList.contains("blue")){
                    cell.classList.remove("blue")
                    cell.classList.add("red")
                    this.computerScore++
                }
                if ((this.userScore && this.computerScore) < this.finishPoint) {
                    this.getGameTimer()
                }
                if (this.userScore === this.finishPoint) {
                    alert("You've won the game")
                    this.stopGame()
                } else if (this.computerScore === this.finishPoint){
                    alert("You've lost the game")
                    this.stopGame()
                }},this.level)
    }


    stopGame() {
            this.playBtn.textContent = "Play"
            this.userScore = 0;
            this.computerScore = 0;
            clearInterval(this.timer)
            this.cells.forEach(elem => {
                elem.classList.remove("blue")
                elem.classList.remove("red")
                elem.classList.remove("green")
            })
    }

    startGame(){
        if (this.playBtn.textContent === "Play"){
            this.playBtn.textContent = "Stop"
            this.userScore = 0;
            this.computerScore = 0;
            this.getGameTimer()
        }

    }


}

const game = new Game(level, playBtn, cells)

game.cells.forEach(function (item) {
    item.addEventListener("click", function () {
        if (item.classList.contains("blue")){
            item.classList.add("green")
            item.classList.remove("blue")
        }

    })
})

game.playBtn.addEventListener("click", function () {
    if (game.playBtn.textContent === "Play"){
        game.startGame();
        game.changeDifficult();
    } else {
        game.stopGame()
    }
})
