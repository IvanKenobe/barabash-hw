fetch("https://swapi.dev/api/films/").
    then(response => response.json().
    then(response => {
        response.results.forEach(elem =>{
            const filmList = document.createElement("ul")
            const filmTitle = document.createElement("li")
            const filmId = document.createElement("li")
            const filmDescription = document.createElement("li")
            const charactersList = document.createElement("ul")
            filmTitle.textContent = elem.title
            filmId.textContent = `Episode ${elem.episode_id}`
            filmDescription.textContent = `Film Description: ${elem.opening_crawl}`
            let arr = []
            elem.characters.forEach(url => {
                fetch(url).
                    then(response => response.json()).
                    then(response => {
                        arr.push(`<li>${response.name}</li>`)
                    if (arr.length === elem.characters.length){
                        charactersList.innerHTML = arr.join("")
                    }
                })
            })
            filmList.append(filmTitle)
            filmList.append(filmId)
            filmList.append(filmDescription)
            filmList.append(charactersList)
            document.body.append(filmList)
        })
    }))