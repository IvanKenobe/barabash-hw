const arr = ['hello', 'world', 'Kiev', 'Kharkiv', 'Odessa', 'Lviv', "Santa" ];

function getArr() {
    const liArray = arr.map(function (elem) {
        return `<li>${elem}</li>`;
    });
    const ul = document.createElement("ul");
    ul.innerHTML = `${liArray.join('')}`;
    document.body.append(ul);
}
console.log(getArr())