let tab = function () {
    let tabNav = document.querySelectorAll(".tabs-title");
    let tabText = document.querySelectorAll(".tab");
    let tabName = "";

    tabNav.forEach(item => item.addEventListener("click", selectTabNav));

    function selectTabNav() {
        tabNav.forEach(item=>{
            item.classList.remove("active")
        });
        this.classList.add("active");
        tabName = this.getAttribute("data-tab-name");

        selectTabContent(tabName);
    }

    function selectTabContent(tabName) {

        tabText.forEach(item=>{
            if(item.classList.contains(tabName))
            {item.classList.add("is-active")
            }
            else{
                item.classList.remove("is-active");
            }
        })
    }
};


tab();