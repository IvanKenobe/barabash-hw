const userPass = document.getElementById("first-input");
const icon = document.getElementById("icon1");

const passChecked = document.getElementById("second-input");
const iconSlash = document.getElementById("icon2");

const confirm = document.getElementById("confirm-btn");

confirm.addEventListener("click", function (e) {
    e.preventDefault()
   if (userPass.value === passChecked.value){
       alert("You're welcome")
   } else {
      const wrongPassword = document.createElement("p");
      wrongPassword.textContent = "Вы ввели неправильное значение";
      wrongPassword.style.color = "red";
       passChecked.after(wrongPassword)
   }
});

icon.addEventListener("click", function () {
    changeInput(userPass, icon)
});

iconSlash.addEventListener("click", function () {
    changeInput(passChecked, iconSlash)
});

function changeInput(tempPass, tempIcon) {
    if (tempIcon.classList.contains("fa-eye")){
        tempIcon.classList.add("fa-eye-slash");
        tempIcon.classList.remove("fa-eye");
        tempPass.type = "text";
    } else {
        tempIcon.classList.remove("fa-eye-slash");
        tempIcon.classList.add("fa-eye");
        tempPass.type = "password"
    }
}
