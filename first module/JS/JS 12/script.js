let counter = 0;
let flag = false;
const images = document.querySelectorAll(".image-to-show");
let stepBrother = setInterval(tick, 2000)

const stopBtn = document.getElementById("stopBtn");
stopBtn.addEventListener("click", function () {
    flag = false;
    clearInterval(stepBrother)
});

const runBtn = document.getElementById("runBtn");
runBtn.addEventListener("click", function (){
    if (flag) {
        return
    }
    flag = true;
    tick();
    stepBrother = setInterval(tick, 2000)
});

function tick() {
    images[counter].classList.remove("show");
    counter++;
    if (counter === images.length) {
        counter = 0;
    }
    images[counter].classList.add("show");
}