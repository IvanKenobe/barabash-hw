const priceWindow = document.getElementById("price");
const priceInput = document.getElementById("price-input");
let price = 0;

priceInput.addEventListener("focus", function () {
    priceWindow.style.border = "5px solid green"
});
priceInput.addEventListener("blur", function () {
    if (this.value < 0){
        priceInput.style.border = "3px solid red";
        const errorMessage = document.createElement("span");
        errorMessage.textContent = `Please enter correct price`;
        priceWindow.after(errorMessage)
    }else {
        priceWindow.style.border = "none";
        price = this.value;
        priceInput.style.backgroundColor = "green";
        let newSpan = document.createElement("span");
        newSpan.innerHTML = `Текущая цена: ${price} <span class="close">X</span>`;
        document.body.append(newSpan);
        newSpan.addEventListener("click", function (e) {
            if (e.target.classList.contains("close")){
                this.remove();
                priceInput.style.backgroundColor = "white";
                price = 0;
                priceInput.value = "0";
            }
        })
    }

});
