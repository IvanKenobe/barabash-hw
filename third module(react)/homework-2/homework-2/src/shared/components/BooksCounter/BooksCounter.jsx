import React from "react"
import "./style.css"


export const BooksCounter = (props) => {
    const {counter} = props
    return(
        <h3 id="book-count" className="book-count mt-5">Всего книг: {counter}</h3>

    )
}