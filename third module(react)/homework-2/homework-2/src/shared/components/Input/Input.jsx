import React from "react";
import "./style.css"


export const Input = (props) => {
    const {name, className, type = "text", handleChange, value, required = false} = props

    return(
        // <div className="form-group">
        //     <label>{label}</label>
        //     <input name={name} className={`form-control ${className}`} type = {type} onChange={handleChange} value = {value} required={required}/>
        // </div>
        <input name={name} type={type} required={required} value={value} className={`form-control ${className}`} onChange={handleChange}/>
    )
}

