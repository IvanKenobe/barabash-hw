import React from "react";
import "./style.css"
import {BookItem} from "../BookItem";


export const BookList = ({bookList, deleteBook, updateBook}) =>{
    console.log(bookList)
    const bookItems = bookList.map((book, index) => <BookItem {...book}
                                                              deleteBook={()=>deleteBook(index)}
                                                              updateBook={(title, author, isbn)=>updateBook(index, title, author, isbn)}/>);
   console.log(bookItems)
    return (
        <table className="table table-striped mt-2">
            <thead>
            <tr>
                <th>Title</th>
                <th>Author</th>
                <th>ISBN#</th>
            </tr>
            </thead>
            <tbody>
            {bookItems}
            </tbody>
        </table>
    )
};