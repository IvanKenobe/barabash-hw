import React from "react"
import "./style.css"

export const Button = (props) => {
    const {text,type,handleClick} = props

    return(
        <button className={`btn ${type}`} onClick={handleClick}>
            {text}
        </button>
    )
}