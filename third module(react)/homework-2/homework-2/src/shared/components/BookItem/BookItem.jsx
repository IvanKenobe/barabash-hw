import React, {Component} from 'react';
import './style.css';
import {Input} from "../Input";
import {Button} from "../Button";

export class BookItem extends Component{
    state ={
        edited: false,
        title:'',
        author:'',
        isbn:''
    };


    handleChange = (event) =>{
        const book = {[event.target.name]: event.target.value};
        this.setState(book);
    };


    editBook = () =>{
        const {title, author, isbn} = this.props;
        this.setState({
            edited: true,
            title,
            author,
            isbn
        });
    };

    finishUpdate = (e) =>{
        e.preventDefault();
        const {title, author, isbn} = this.state;
        this.props.updateBook(title, author, isbn);
        this.setState({
            edited: false
        });
    };

    render() {
        if (this.state.edited) {
            const {title, author, isbn} = this.state;
            return (
                <tr>
                    <td>
                        <div className="form-group">
                            <Input type="text" name="title" value={title} required handleChange={this.handleChange}/>
                        </div>
                    </td>
                    <td>
                        <div className="form-group">
                            <Input type="text" name="author" value={author} required handleChange={this.handleChange}/>
                        </div>
                    </td>
                    <td>
                        <div className="form-group">
                            <Input type="text" name="isbn" value={isbn} required handleChange={this.handleChange}/>
                        </div>
                    </td>
                    <td><Button type="primary" text="Update" handleClick={this.finishUpdate}/>
                    </td>
                </tr>
            )
        }
        else {
            const {title, author, isbn, deleteBook} = this.props;
            return (
                <tr>
                    <td>{title}</td>
                    <td>{author}</td>
                    <td>{isbn}</td>
                    <td><a href="#" className="btn btn-info btn-sm" onClick={this.editBook}><i className="fas fa-edit"></i></a></td>
                    <td><a href="#" className="btn btn-danger btn-sm btn-delete" onClick={deleteBook}>X</a></td>
                </tr>
            )
        }
    }
}