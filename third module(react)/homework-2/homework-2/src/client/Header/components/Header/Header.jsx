import React from 'react';
import './style.css';
export const Header = (props) =>{
    const {firstWord, secondWord, description} = props;
    return (
        <div className='header'>
            <h1 className="display-4 text-center"><span className='title'>{firstWord}</span>{secondWord}</h1>
            <p className='text-center'>{description}</p>
        </div>
    )
};