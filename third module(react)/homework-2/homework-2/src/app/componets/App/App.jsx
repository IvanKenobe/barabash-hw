import React from 'react';
import 'bootstrap/dist/css/bootstrap.min.css';
import {Header} from "../../../client/Header/components/Header"
import {Main} from "../../../client/Main/components/Main";



export const App = () => {
    const pageProps ={
        header:{
            firstWord:'Book',
            secondWord:'List',
            description:'Add your book information to store it in database.'
        }
    };
    return (
        <div className="container">
            <Header {...pageProps.header}/>
            <Main />
        </div>
    );
};
