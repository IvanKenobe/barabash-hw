import React from 'react';
// import 'bootstrap/dist/css/bootstrap.min.css';
// import '../../../shared/styles/scss/style.scss';

// if you using css deleting scss import, and uncomment this import
import "./style.css"
import {Header} from  "../../../client/Header/components/Header"
import {MovieList} from '../../../client/MovieList/components/MovieList';
import {Footer} from "../../../client/Footer/components/Footer"

  const pageProps = {
    navbar: {
      logo: {
        firstWord: "Movie",
        secondWord: "Rise",
        type: ""
      },
    },
    content: {
      name: "The Jungle Book",
      genres: ["Adventure", "Drama", "Family", "Fantasy"],
      duration: "1h 46m",
      rating: 4.8
    },
    sortNavbar:[
      'Trending',
      'Top Rated',
      'New Arrivals',
      'Trailers',
      'Coming Soon',
      'Genre'
    ],
    footer:{
      logo:{
        firstWord: "Movie",
        secondWord: "Rise",
        type: "color"
      },
      footerMenu:[
        {href:'#', text: 'About'},
        {href:'#', text: 'Terms of Service'},
        {href:'#', text: 'Contact'}
      ],
      footIcons:[
        {href:"#", src: "Facebook@1X.png"},
        {href:"#", src: "Twitter@1X.png"},
        {href:"#", src: "Pinterest@1X.png"},
        {href:"#", src: "Instagram@1X.png"},
        {href:"#", src: "YouTube@1X.png"},
      ]
    }
  }

export const App = () => {
    const {footer,sortNavbar,...header} = pageProps
    return (
        <div className= "">
          <Header {...header}/>
          <MovieList sortNavbar = {sortNavbar}/>
          <Footer {...footer}/>
        </div>
    );
};
