import React from "react";
import {Navbar} from "../../../Navbar/components/Navbar"
import "./style.css"
import { FilmContent } from '../../../../shared/components/FilmContent';


export const Header = (props) => {
  const {navbar,content} = props
  return(
    <div className="header" >
      <Navbar{...navbar}/>
      <FilmContent {...content}/>
    </div>
  )
}