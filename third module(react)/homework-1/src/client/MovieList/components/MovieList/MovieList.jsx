import React from "react"
import "./style.css"
import {MovieCard} from "../../../../shared/components/MovieCard"
import {SortNavbar} from '../../../../shared/components/SortNavbar';
import {Banner} from "../../../../shared/components/Banner"
import movieList from './dumpdata';



const {  moveCards, bannerRow } = movieList;


export const MovieList = (props) => {
  const {sortNavbar} = props
  const cards = moveCards.map(item => <MovieCard {...item} />);
  const movieRowBefore = cards.slice(0, bannerRow * 4);
  const movieRowAfter = cards.slice(bannerRow * 4);
  return(
   <div className="movie-list">
     <div className="container">
       <SortNavbar sortList = {sortNavbar}/>
       <div className='move-list-content'>
         {movieRowBefore}
         <Banner/>
         {movieRowAfter}
       </div>
     </div>
   </div>
  )
}