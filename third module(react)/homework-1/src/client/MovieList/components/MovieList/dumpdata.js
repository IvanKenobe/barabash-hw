// import firstImage from "../../../../../public/firstImage.png"

const dumpdata = {
  bannerRow: 3,
  moveCards: [
    {
      src: "firstImage.png",
      name: 'Fantastic Beasts...',
      rating: 4.7,
      category: ['Adventure', ', Family', ', Fantasy'],
    },
    {
      src: "secondImage.png",
      name: 'Alice Through th...',
      rating: 4.1,
      category: ['Adventure', ', Family', ', Fantasy'],
    },
    {
      src: "thirdImage.png",
      name: 'AssAssin’s Creed',
      rating: 4.2,
      category: ['Action', ', Adventure', ', Fantasy'],
    },
    {
      src: "fourthImage.png",
      name: 'Captain America...',
      rating: 4.9,
      category: ['Action', ', Adventure' , ', Sci-Fi'],
    },
    {
      src: "fifthImage.png",
      name: 'Doctor Strange',
      rating: 4.8,
      category: ['Action', ', Adventure', ', Fantasy'],
    },
    {
      src: "sixImage.png",
      name: 'Doctor Strange',
      rating: 4.8,
      category: ['Action', ', Adventure', ', Fantasy'],
    },
    {
      src: "sevenImage.png",
      name: 'Fantastic Beasts...',
      rating: 4.7,
      category: ['Adventure', ', Family', ', Fantasy'],
    },
    {
      src: "nineImage.png",
      name: 'Fantastic Beasts...',
      rating: 4.7,
      category: ['Adventure', ', Family', ', Fantasy'],
    },
    {
      src: "eightImage.png",
      name: 'Finding Dory',
      rating: 4.7,
      category: ['Animation', ', Adventure', ', Comedy'],
    },
    {
      src: "tenImage.png",
      name: 'Moana',
      rating: 4.9,
      category: ['Action', ', Fantasy'],
    },
    {
      src: "elevenImage.png",
      name: 'Ice Age: Collisio...',
      rating: 4.5,
      category: ['Adventure', ', Comedy'],
    },
    {
      src: "twelveImage.png",
      name: 'Independence Day',
      rating: 3.9,
      category: ['Action', ', Sci-Fi'],
    },
    {
      src: "thirdImage.png",
      name: 'Turtles',
      rating: 4.7,
      category: ['Adventure', ', Family', ', Fantasy'],
    },
    {
      src: "sixImage.png",
      name: 'Now you see me 2',
      rating: 4.4,
      category: ['Action, Adventure, Comedy'],
    },
    {
      src: "firstImage.png",
      name: 'Fantastic Beasts...',
      rating: 4.7,
      category: ['Adventure', ', Family', ', Fantasy'],
    },
    {
      src: "secondImage.png",
      name: 'The BFG',
      rating: 3.2,
      category: ['Adventure', ', Family', ', Fantasy'],
    },
  ],
};
export default dumpdata;