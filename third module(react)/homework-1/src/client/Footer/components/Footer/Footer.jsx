import React from "react"
import './style.css'
import {FooterMenu} from '../../../../shared/components/FooterMenu';
import {Logo} from "../../../../shared/components/Logo";
import {FooterIcons} from '../../../../shared/components/FooterIcons/FooterIcons';
import {CopyRight} from '../../../../shared/components/CopyRight/CopyRight';


export const Footer = (props) => {
  const {logo,footerMenu,footIcons} = props
  // console.log({icons})
  return(
    <div className="footer">
      <div className="container">
        <div className="footer-row">
          <FooterMenu footerMenu={footerMenu}/>
          <Logo {...logo}/>
          <FooterIcons footIcons = {footIcons}/>
        </div>
        <div className="footer-copyright">
          <CopyRight/>
        </div>
      </div>
    </div>
  )
}