import React from "react";
import {Logo} from "../../../../shared/components/Logo"
import {AutoPanel} from "../../../../shared/components/AutoPanel"
import "./style.css"

export const Navbar = (props) => {
  const {logo} = props
  return(
    <div className="navbar">
      <div className="container">
        <Logo {...logo}/>
        <div className='search-icon'><i className="fas fa-search" /></div>;
        <AutoPanel/>
      </div>
    </div>
  )
}