import React from "react";
import "./style.css"

export const Button = (props) => {
  const {text,type} = props
  return(
    <button className={`btn ${type}`}>
      {text}
    </button>
  )
}