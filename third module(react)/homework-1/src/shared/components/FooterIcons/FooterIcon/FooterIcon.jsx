import React from "react";
import "./style.css"


export const FooterIcon = (props) => {
  const {href,src} = props
  return(
    <a href={href} className='footer-icon'><img src={src}/></a>

  )
}