import React from "react";
import "./style.css"
import {FooterIcon} from './FooterIcon';

export const FooterIcons = (props) =>{
  const {footIcons} = props
  console.log(footIcons)
  const iconList = footIcons.map(({href,src}) => <FooterIcon href = {href} src = {src}/>)
  return(
    <>
      <div className="social-icons">
        {iconList}
      </div>
    </>
  )
}