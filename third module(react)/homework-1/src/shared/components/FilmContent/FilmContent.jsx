import React from "react"
import "./style.css"
import {Button} from "../Button"

export const FilmContent = (props) => {
  const {name, genres, duration, rating} = props
  const genresItems = genres.map(item=><li className='genre-item'>{item}</li>);
  const ratingStars = Array(Math.round(rating)).fill(<span className='star-item'>&#9733;</span>)

  return(
    <div className="container">
      <div className="film-content">
        <h1 className="content-title">{name}</h1>
        <div>
          <ul className="genre-list">
            {genresItems}
            {duration}
          </ul>
        </div>
        <div className="header-footer">
          <div className="stars">
            {ratingStars}
            <span className="movie-rating rating">{rating}</span>
          </div>
          <div className="film-content-buttons">
            <Button text ="Watch Now" type ='btn-primary'/>
            <Button text ='View Info' type ='btn-view-info'/>
            <Button text ='+ Favorites' type ='btn-secondary'/>
          </div>

        </div>
      </div>
    </div>
  )
}