import React from "react"
import "./style.css"

export const MovieCard = (props) =>{
  const {src,name,rating,category} = props
  const categoryLinks = category.map(item => <a href="#" className="movie-card-item">{item}</a>)

  return(
    <div className="movie-card">
      <img className='movie-img' src={src}/>
      <div className='movie-info'>
        <h4 className='movie-name'>{name}</h4>
        <span className='movie-rating'>{rating}</span>
      </div>
      <div className='movie'>
        {categoryLinks}
      </div>
    </div>
  )
}