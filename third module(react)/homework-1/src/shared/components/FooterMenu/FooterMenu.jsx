import React from "react"
import "./style.css"


export const FooterMenu = (props) => {
  const {footerMenu} = props
  const menu = footerMenu.map(({text,href}) => <li className="footer-menu-item"><a className="footer-menu-link" href={href}>{text}</a></li>)
  return(
    <ul className="footer-menu">
      {menu}
    </ul>
  )
}