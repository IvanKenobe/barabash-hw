import React from "react"
import "./style.css"


export const SortNavbar = (props) => {
  const {sortList} = props
  const sortNavbar = sortList.map(item => <li className="nav-item"><a className="nav-link">{item}</a></li>)

  return(
    <div className='sort-navbar'>
      <ul className='navbar-container'>
        {sortNavbar}
      </ul>
    </div>
  )
}