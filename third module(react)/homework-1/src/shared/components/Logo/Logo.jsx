import React from "react";
import "./style.css"

export const Logo = (props) => {
  const {firstWord,secondWord,type} = props
  const className = (type === "color") ? "logo text-colored" : "logo text-white"
  return(
   <div>
     <a className={className}>
       <span className="logo text-bold">
         {firstWord}
       </span>
       <span className="logo ">
         {secondWord}
       </span>
     </a>
   </div>
  )
}
