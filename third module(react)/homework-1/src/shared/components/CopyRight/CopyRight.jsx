import React from "react"
import "./style.css"


export const CopyRight = () => {
  return(
    <p className='copyright-text'>Copyright © 2017 <span className="text-bold">MOVIE</span>RISE. All Rights Reserved.</p>
  )
}