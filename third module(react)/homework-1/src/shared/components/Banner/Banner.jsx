import React from "react";
import "./style.css"
import {Button} from "../Button"

export const Banner = () => {
  return(
    <div className="banner-section">
      <div className="banner-bgc-black"></div>
      <div className="banner-container">
        <h2 className="banner-text">
          Receive information on the latest hit movies straight to your inbox.
        </h2>
        <div className="banner-button">
          <Button text="Subscribe" type="btn-view-info"/>
        </div>
      </div>
    </div>
  )
}