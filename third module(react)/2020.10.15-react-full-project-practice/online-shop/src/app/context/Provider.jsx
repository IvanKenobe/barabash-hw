import React,{useReducer} from "react"
import {context} from "./context";
import {initialContext} from "./initialContext";
import {SET_CURRENCY,SET_LANG} from "./constants";
import {reducer} from "./reducer";


const ContextProdiver = ({children}) => {
    const [state,dispatch] = useReducer(reducer,initialContext)

    const setCurrency = (newCurrency) => {dispatch({type: SET_CURRENCY,payload: newCurrency})}

    const setLang = (newLang) => {dispatch({type: SET_LANG,payload: newLang})}

    return(
        <context.Provider value={{state,setCurrency,setLang}}>
            {children}
        </context.Provider>
    )
}

ContextProdiver.defaultProps = {
    children: ""
}


export default ContextProdiver;
