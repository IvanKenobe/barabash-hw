
const validationsMessage = {
    required: 'Поле обязательно к заполнени',
    email: 'в email обязательно должна быть @ и не должно быть пробелов',
}

export default validationsMessage;