import React from "react"
import {Formik, Field, Form, ErrorMessage} from "formik";
import validationsMessage from "./validation";
import * as yup from 'yup';


const fields = {
    firstName: {
        name: "name",
        placeholder: "Name"
    },
    lastName: {
        name: "lastName",
        placeholder: "Last Name"
    },
    patronymic: {
        name: "patronymic",
        placeholder: "Patronymic"
    },
    phone: {
        name: "phone",
        placeholder:"Phone"
    },
    email:{
        name: "email",
        placeholder:"Email",
        type: "email"
    }
}



const RegistrationForm = () => {

    const onSubmit = (value) => {
        console.log(value)
    }

    const initialState ={
        firstName: "",
        lastName:"",
        patronymic:"",
        phone:'',
        email:"",
    }

    // let yup = require("yup")


    // const registrationSchema = yup.object({
    //     name: yup.string().required(validationsMessage.required),
    //     lastName: yup.string().required(validationsMessage.required),
    //     patronymic: yup.string().required(validationsMessage.required),
    //     email: yup.string().email(validationsMessage.email).required(validationsMessage.required)
    // })

    const formProps = {
        initialState,
        onSubmit,
        // registrationSchema
    }
    return(
        <Formik {...formProps}>
            <Form>
                <Field {...fields.firstName}/>
                <Field {...fields.lastName}/>
                <Field {...fields.patronymic}/>
                <Field {...fields.phone}/>
                <Field {...fields.email}/>
            </Form>
        </Formik>
    )
}


export default RegistrationForm;