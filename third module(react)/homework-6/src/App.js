import React from 'react';
import RegistrationForm from "./shared/components/RegistrationForm/RegistrationForm";

function App() {
  return (
    <div className="App">
      <RegistrationForm/>
    </div>
  );
}

export default App;
