$(".tabs-caption .tab-item").click(function () {
    $(this).siblings().removeClass("active");
    $(this).addClass("active");
    const index = $(this).index();
    $(".tabs-content").removeClass("active").eq(index).addClass("active")

});

$(".our-work li").click(function () {
    $(this).siblings().removeClass("active");
    $(this).addClass("active");
    $(".our-work-item").hide("slow");
    const selector = $(this).data("target");
    $(`.our-work-item${selector}`).show("slow");
    if ($(".true").hasClass("active")){
        $("#show-more").css("display", "flex")
    } else {
        $("#show-more").css("display", "none")
    }
});

$(".our-work-item").hide().slice(0,12).show();

$("#show-more").on("click", function () {
    const select = $(this).data("target");
    $(`${select}:hidden`).slice(0,12).show();
    $("#hide").css("display", "block")
});

$("#hide").on("click", function () {
   $(".our-work-item").hide().slice(0,12).show();
    $("#hide").css("display","none")
});

const sliders = $(".slide-content");
let counter = 0;
const userList = $(".authors-list-img");
const leftBtn = $(".slider-left-btn");
const rightBtn = $(".slider-right-btn");
let activeElemUser = null;
let activeElemSlide = null;

leftBtn.on("click", function () {

    userList.removeClass("authors-list-img-active");
    sliders.removeClass("slide-content-active");
    counter--;

    if (counter === -1){
        counter = userList.length - 1
    }
    console.log(counter);

    $(userList[counter]).addClass("authors-list-img-active");
    $(sliders[counter]).addClass("slide-content-active")
});
rightBtn.on("click", function () {
    userList.removeClass("authors-list-img-active");
    sliders.removeClass("slide-content-active");
    counter += 1;

    if (counter === userList.length){
        counter = 0
    }
    $(userList[counter]).addClass("authors-list-img-active");
    $(sliders[counter]).addClass("slide-content-active")
});
userList.on("click", function () {
    activeElemUser = $('.authors-list-img-active')[0];
    activeElemSlide = $('.slide-content-active')[0];
    const elementIndex = $(this).data('target');
    counter = elementIndex;
    $(activeElemSlide).removeClass('slide-content-active');
    $(activeElemUser).removeClass('authors-list-img-active');
    console.log(elementIndex);
    $(userList[elementIndex]).addClass('authors-list-img-active');
    $(sliders[elementIndex]).addClass('slide-content-active');
});



